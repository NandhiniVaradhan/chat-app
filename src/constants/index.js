import {FONTS, SIZES, COLORS, Fonts, adjust} from './Theme';
import Images from './Images';
import Vector from './Vector';
import {PlaceHolders} from './strings';

export {FONTS, Fonts, COLORS, SIZES, Vector, Images, PlaceHolders, adjust};
