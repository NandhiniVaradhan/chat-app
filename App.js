import {Text, View, Image} from 'react-native';
import React from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, Vector, Images, PlaceHolders} from './src/constants';

const App = () => {
  return (
    <View style={styles.container}>
      <Image
        style={{height: 100, width: 200}}
        resizeMode={'contain'}
        source={Images.logo}
      />
      <Text style={styles.txt}>{PlaceHolders.welcome}</Text>
    </View>
  );
};

export default App;

const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    backgroundColor: COLORS.blackColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {
    color: COLORS.secondaryColor,
    ...FONTS.h1,
  },
});
